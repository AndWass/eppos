
set(BOARD_INC ${CMAKE_CURRENT_LIST_DIR}/board/${EPPOS_BOARD}/board.cmake)
if(EXISTS ${BOARD_INC})
    include(${CMAKE_CURRENT_LIST_DIR}/board/${EPPOS_BOARD}/board.cmake)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -T ${BOARD_LINK_DEPENDS}")
elseif(NOT EXISTS ${CMAKE_CURRENT_LIST_DIR}/board/${EPPOS_BOARD})
    message(FATAL_ERROR "The board '${EPPOS_BOARD}' doesn't exist")
endif()
