function(set_arch ARCH TASKING_TYPE)
    set(ARCH_DIR "src/arch/${ARCH}")
    set(TCB_DIR ${ARCH_DIR})
    if("${ARCH}" STREQUAL "cortex-m4")
        set(TCB_DIR "src/arch/cortex-m34")
        set(ARCH_DIR "src/arch/cortex-m34")
    endif()
    set(ARCH_SRC
        "${TCB_DIR}/task_control_block.cpp"
        "${ARCH_DIR}/system_tick.cpp"
        PARENT_SCOPE)
endfunction()