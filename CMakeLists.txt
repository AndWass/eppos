cmake_minimum_required(VERSION 3.1)
project(eppos)
enable_language(C CXX ASM)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
set(EPPOS_BOARD "atsam4sd32c")
set(CXX_STANDARD 17)

include(board.cmake)
include(driver.cmake)
include(arch.cmake)
include(core.cmake)

#find_driver("i2c:i2c")
#find_driver("display:ssd0303")
set_arch("cortex-m4" "cooperative")

add_executable(eppos
    app/main.cpp
    app/retarget.cpp
    ${ARCH_SRC}
    ${BOARD_SRC}
    ${DRIVER_SOURCES}
    ${CORE_SOURCES}
)
target_include_directories(eppos PUBLIC
    ${CMAKE_CURRENT_LIST_DIR}/include
    ${CMAKE_CURRENT_LIST_DIR}/external/GSL/include
    ${CMAKE_CURRENT_LIST_DIR}/external/boost/include
    ${CMAKE_CURRENT_LIST_DIR}/external/SG14/SG14
    ${ARCH_INCLUDE_DIRS}
)
target_include_directories(eppos PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/app
    ${BOARD_INCLUDE_DIRS}
    )
target_compile_definitions(eppos PRIVATE -DBOOST_NO_EXCEPTIONS=1
    ${BOARD_DEFINES})

if(build_benchmarks)
    add_subdirectory(benchmark)
endif()