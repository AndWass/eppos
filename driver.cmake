function(find_driver DRIVER_NAME)
    string(REPLACE ":" "/" DRIVER_NAME ${DRIVER_NAME})
    set(CPP_CANDIDATE "${CMAKE_CURRENT_LIST_DIR}/src/driver/${DRIVER_NAME}.cpp")
    if(EXISTS ${CPP_CANDIDATE})
        set(DRIVER_SOURCES ${DRIVER_SOURCES} ${CPP_CANDIDATE} PARENT_SCOPE)
    endif()
endfunction(find_driver)