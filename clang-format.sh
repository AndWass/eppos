#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd "$DIR/app"
find . -iname "*.cpp" -o -iname "*.hpp" -o -iname "*.h" | xargs clang-format -style=file -i

cd "$DIR/board"
find . -iname "*.cpp" -o -iname "*.hpp" -o -iname "*.h" | xargs clang-format -style=file -i

cd "$DIR/include"
find . -iname "*.cpp" -o -iname "*.hpp" -o -iname "*.h" | xargs clang-format -style=file -i

cd "$DIR/src"
find . -iname "*.cpp" -o -iname "*.hpp" -o -iname "*.h" | xargs clang-format -style=file -i