#pragma once

#include <functional>

namespace eppostd
{
template <class... T>
class function_ref;

template <class R, class... Args>
class function_ref<R(Args...)>
{
    R (*fn_ptr_invoker)(void *, void *, Args &&...);
    void *maybe_class = nullptr;
    void *fn_ptr = nullptr;

public:
    function_ref(R (*fn)(Args...)) : fn_ptr(fn) {
        fn_ptr_invoker = [](void *fn, void *, Args &&... args) {
            return reinterpret_cast<R (*)(Args...)>(fn)(std::forward<Args>(args)...);
        };
    }

    template <class T>
    function_ref(T *, R (T::*fn)(Args...)) {
        fn_ptr_invoker = [](void *fn, void *cl, Args &&... args) -> R {
            auto *p = reinterpret_cast<R (T::*)(Args...)>(fn);
            return std::invoke(*p, cl, std::forward<Args>(args)...);
        };
    }
};
} // namespace eppostd