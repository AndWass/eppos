#pragma once

#include <boost/intrusive/list.hpp>

namespace eppostd
{
template <class T>
using intrusive_list = boost::intrusive::list<T>;

using intrusive_list_base_hook = boost::intrusive::list_base_hook<
    boost::intrusive::link_mode<boost::intrusive::link_mode_type::normal_link>>;
} // namespace eppostd