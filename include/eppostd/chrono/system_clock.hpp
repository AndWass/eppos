#pragma once

#include <chrono>

#include "clock.hpp"

namespace eppostd::chrono
{
class system_clock;
namespace detail
{
using system_clock_impl = eppostd::chrono::clock<system_clock>;
}
class system_clock
{
public:
    using rep = detail::system_clock_impl::rep;
    using period = detail::system_clock_impl::period;
    using duration = detail::system_clock_impl::duration;

    using time_point = std::chrono::time_point<system_clock>;

    static constexpr bool is_steady = true;

    static time_point now() noexcept {
        return time_point{detail::system_clock_impl::value()};
    }
};
} // namespace eppostd::chrono