#pragma once

#include <chrono>

namespace eppostd::chrono
{
template <class Tag, class Rep = std::int64_t, class Period = std::micro>
class clock
{
public:
    using rep = Rep;
    using period = Period;
    using duration = std::chrono::duration<Rep, Period>;

    static duration &value() {
        static duration val_;
        return val_;
    }
};
} // namespace eppostd::chrono