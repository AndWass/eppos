#pragma once

#include "system_clock.hpp"
#include <chrono>

// Always expotr chrono literals from chrono!
using namespace std::chrono_literals;

namespace eppostd::chrono
{
/**
 * @brief Calculate how many ticks a duration represents given a frequency
 * @param frequency The frequency of ticks, in Hz
 * @param duration  The duration to convert to ticks
 *
 * This is typically used to calculate how many ticks a hardware
 * timer should tick for it to run with a specified period.
 *
 * No sophisticated rounding is done, a simple "floor" is taken.
 */
template <class Ratio = std::micro>
std::uint64_t duration_to_ticks(std::uint64_t frequency,
                                std::chrono::duration<std::int64_t, Ratio> duration) {
    // x/frequency = timeout => x = frequency*timeout
    // and we also need to scale ticks with Ratio::num and Ratio::den
    return ((frequency * duration).count()) * Ratio::num / Ratio::den;
}
} // namespace eppostd::chrono