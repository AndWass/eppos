#pragma once

#define SG14_INPLACE_FUNCTION_THROW(x) __builtin_unreachable()
#include "inplace_function.h"

namespace eppostd
{
template <class Signature>
using function = stdext::inplace_function<Signature, 32>;

template <class Signature>
using small_function = stdext::inplace_function<Signature, 16>;

template <class Signature>
using large_function = stdext::inplace_function<Signature, 128>;
} // namespace eppostd
