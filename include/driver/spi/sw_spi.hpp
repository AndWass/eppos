#pragma once

#include <driver/gpio/pin.hpp>
#include <gsl/span>

namespace driver::spi
{
template <gpio::GpioPin ClkPin, gpio::GpioPin MosiPin, gpio::GpioPin MisoPin>
class sw_spi
{
    ClkPin clk_;
    MosiPin mosi_;
    MisoPin miso_;

public:
    sw_spi(ClkPin clk, MosiPin mosi, MisoPin miso) : clk_(clk), mosi_(mosi), miso_(miso) {
    }

    template <gpio::GpioPin ChipSelectPin>
    void write(ChipSelectPin cs_pin, gsl::span<std::uint8_t> write_buffer,
               gsl::span<std::uint8_t> read_buffer) {
        clk_.set_value(false);
        cs_pin.set_value(false);
        // auto read_iter = read_buffer.begin();
        for (int b = 0; b < write_buffer.size(); b++) {
            auto byte = write_buffer[b];
            std::uint8_t rx_byte = 0;
            for (int i = 7; i >= 0; i--) {
                rx_byte <<= 1;
                if (byte & (1 << i)) {
                    mosi_.set_value(true);
                }
                else {
                    mosi_.set_value(false);
                }
                clk_.set_value(true);
                if (miso_.value()) {
                    rx_byte |= 0x01;
                }
                clk_.set_value(false);
            }
            /*if(read_iter != read_buffer.end())
            {
                *read_iter = rx_byte;
                read_iter++;
            }*/
        }
        cs_pin.set_value(true);
    }
};

template <gpio::GpioPin ClkPin, gpio::GpioPin MosiPin, gpio::GpioPin MisoPin>
auto make_sw_spi(ClkPin clk, MosiPin mosi, MisoPin miso) {
    return sw_spi<ClkPin, MosiPin, MisoPin>{clk, mosi, miso};
}

} // namespace driver::spi