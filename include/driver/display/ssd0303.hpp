#pragma once

#include <array>
#include <driver/gpio/concept.hpp>
#include <driver/i2c/i2c.hpp>

namespace driver::display
{
namespace ssd1306
{
template <class SpiBus, driver::gpio::GpioPin ChipSelect, driver::gpio::GpioPin DataCommand>
struct spi_4wire
{
    SpiBus &spi_bus_;
    ChipSelect cs_;
    DataCommand dc_;

    void write_command(std::uint8_t command) {
        dc_.set_value(false);
        spi_bus_.write(cs_, {&command, 1}, {});
    }

    void write_command(gsl::span<std::uint8_t> command) {
        dc_.set_value(false);
        spi_bus_.write(cs_, command, {});
    }

    void write_data(gsl::span<std::uint8_t> data) {
        dc_.set_value(true);
        spi_bus_.write(cs_, data, {});
    }
};

template <class ComImpl>
class ssd1306
{
    ComImpl com_impl_;
    using row_type = std::array<std::uint8_t, 128>;
    // std::array<row_type, 4> rows;
public:
    ssd1306(ComImpl com) : com_impl_(std::move(com)) {
    }
    void setup() {
        com_impl_.write_command(0x8D);
        com_impl_.write_command(0x14);
        com_impl_.write_command(0xAF);
        com_impl_.write_command(0xA5);
        set_contrast(0x8f);
        // com_impl_.write_command(0xA6);
    }

    void set_contrast(std::uint8_t contrast) {
        std::array<std::uint8_t, 2> data{0x81, contrast};
        com_impl_.write_command(data);
    }
};
template <class SpiBus, driver::gpio::GpioPin ChipSelect, driver::gpio::GpioPin DataCommand>
auto make_ssd1306(SpiBus &spi, ChipSelect cs, DataCommand dc) {
    spi_4wire<SpiBus, ChipSelect, DataCommand> spi_impl{spi, cs, dc};
    return ssd1306<spi_4wire<SpiBus, ChipSelect, DataCommand>>{spi_impl};
}
} // namespace ssd1306
struct ssd0303
{
    driver::i2c::master::chip chip;
    std::array<std::uint8_t, 132 + 1> pixel_command_buffer[2];

    ssd0303(std::uint8_t i2c_address, driver::i2c::master *i2c_bus);

    void setup();
    void turn_off();
    void turn_on();
    void send_command(std::uint8_t command);
    void set_pixel(unsigned int x, unsigned int y, bool on);
    void update();
};
} // namespace driver::display