#pragma once

#include <cstdint>
#include <driver/peripheral/concept.hpp>
#include <utility>

namespace driver::irq
{
template <class T>
concept bool IrqPeripheral = requires(T t) {
    requires(driver::peripheral::Peripheral<T>);
    {t.enable_irqs(std::uint32_t{})};
    {t.disable_irqs(std::uint32_t{})};
    {t.make_subscription(std::uint32_t{}, (void (*)(std::uint32_t)){})};
    {std::declval<typename T::irq_subscription>()};
    {&T::subscribe_irq};
};
} // namespace driver::irq