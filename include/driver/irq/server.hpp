#pragma once

#include <atomic> // Needed for atomic_signal_fence
#include <boost/intrusive/list.hpp>
#include <eppostd/function.hpp>

namespace driver::irq
{
template <class SubscriptionSignature>
struct subscription
    : public boost::intrusive::list_base_hook<
          boost::intrusive::link_mode<boost::intrusive::link_mode_type::normal_link>>
{
    using fn_type = SubscriptionSignature;
    subscription(std::uint32_t mask, const eppostd::small_function<fn_type> &target_fn)
        : mask_(mask), target_fn_(target_fn) {
    }
    std::uint32_t mask_ = 0;
    eppostd::small_function<fn_type> target_fn_;
};

template <class Subscription>
struct server
{
    server() = default;
    boost::intrusive::list<Subscription> subs;
    Subscription
    make_subscription(std::uint32_t mask,
                      eppostd::small_function<typename Subscription::fn_type> target_fn) {
        return Subscription{mask, target_fn};
    }
    void subscribe(Subscription &sub) {
        subs.push_back(sub);
        std::atomic_signal_fence(std::memory_order_release);
    }
};
} // namespace driver::irq