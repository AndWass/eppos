#pragma once

#include "concept.hpp"

namespace driver::gpio::pin
{
template <GpioPeripheral Periph>
class pin
{
    Periph &chip_;
    std::uint32_t mask_;

public:
    pin(Periph &chip, std::uint32_t pin) : chip_(chip), mask_(1 << pin) {
    }
    void configure_direction(direction dir) {
        chip_.configure_direction(mask_, dir);
    }
    void configure_pull(pull_config config) {
        chip_.configure_pull(mask_, config);
    }
    void set_value(bool value) {
        chip_.set_values(mask_, value);
    }
    bool value() {
        return chip_.values() & mask_;
    }
    void toggle() {
        set_value(!value());
    }

    void enable_irq() requires driver::irq::IrqPeripheral<Periph> {
        chip_.enable_irqs(mask_);
    }

    void disable_irq() requires driver::irq::IrqPeripheral<Periph> {
        chip_.disable_irqs(mask_);
    }
};

template <GpioPeripheral Periph>
auto make_pin(Periph &periph, std::uint32_t pin_nr) {
    return pin<Periph>{periph, pin_nr};
}
} // namespace driver::gpio::pin