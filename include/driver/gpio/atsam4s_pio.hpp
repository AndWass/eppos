#pragma once

#include "concept.hpp"
#include <core/mem_access.hpp>
#include <driver/irq/server.hpp>

namespace driver::gpio
{
namespace atsam4s_pio_base
{
constexpr std::uint32_t pio_a = 0x400e0e00, pio_b = 0x400e1000, pio_c = 0x400e1200;
}

namespace gpio = driver::gpio;
template <std::uintptr_t Base>
class atsam4s_pio
{
    struct offsets
    {
        constexpr static std::uintptr_t enable = 0, disable = 4, status = 8, output_enable = 0x10,
                                        output_disable = 0x14, output_status = 0x18,
                                        enable_glitch_filter = 0x20, set_output_data = 0x30,
                                        clear_output_data = 0x34, output_data_status = 0x38,
                                        pin_data_status = 0x3c, irq_enable = 0x40,
                                        irq_disable = 0x44, irq_status = 0x4c,
                                        pullup_disable = 0x60, pullup_enable = 0x64,
                                        enable_debounce_filter = 0x84, debounce_div = 0x8c,
                                        pulldown_disable = 0x90, pulldown_enable = 0x94,
                                        output_write_enable = 0xA0,
                                        enable_additional_irq_modes = 0xB0, set_level_irq = 0xc4;
    };

    // This is a singleton! use instance() to get an actual instance!
    atsam4s_pio() = default;
    atsam4s_pio(const atsam4s_pio &) = delete;
    atsam4s_pio(atsam4s_pio &&) = delete;

    atsam4s_pio &operator=(const atsam4s_pio &) = delete;
    atsam4s_pio &operator=(atsam4s_pio &&) = delete;

    irq::server<irq::subscription<void(std::uint32_t)>> irq_server;

public:
    using irq_subscription = irq::subscription<void(std::uint32_t)>;
    static atsam4s_pio &instance() noexcept {
        static atsam4s_pio inst;
        return inst;
    }

    void enable() noexcept {
        if constexpr (Base == atsam4s_pio_base::pio_a) {
            core::mem_access::write<0x400e0410>(1 << 11);
        }
        else if (Base == atsam4s_pio_base::pio_b) {
            core::mem_access::write<0x400e0410>(1 << 12);
        }
        else {
            core::mem_access::write<0x400e0410>(1 << 13);
        }
    }
    void disable() noexcept {
        if constexpr (Base == atsam4s_pio_base::pio_a) {
            core::mem_access::write<0x400e0414>(1 << 11);
        }
        else if (Base == atsam4s_pio_base::pio_b) {
            core::mem_access::write<0x400e0414>(1 << 12);
        }
        else {
            core::mem_access::write<0x400e0414>(1 << 13);
        }
    }
    void configure_direction(std::uint32_t mask, gpio::pin::direction dir) noexcept {
        if (dir == gpio::pin::direction::output) {
            core::mem_access::write<Base + offsets::enable>(mask);
            core::mem_access::write<Base + offsets::output_enable>(mask);
            core::mem_access::write<Base + offsets::output_write_enable>(mask);
        }
        else {
            core::mem_access::write<Base + offsets::enable>(mask);
            core::mem_access::write<Base + offsets::output_disable>(mask);
        }
    }

    void configure_pull(std::uint32_t mask, gpio::pin::pull_config cfg) {
        if (cfg == gpio::pin::pull_config::none) {
            core::mem_access::write<Base + offsets::pulldown_disable>(mask);
            core::mem_access::write<Base + offsets::pullup_disable>(mask);
        }
        else if (cfg == gpio::pin::pull_config::up) {
            core::mem_access::write<Base + offsets::pulldown_disable>(mask);
            core::mem_access::write<Base + offsets::pullup_enable>(mask);
        }
        else {
            core::mem_access::write<Base + offsets::pullup_disable>(mask);
            core::mem_access::write<Base + offsets::pulldown_enable>(mask);
        }
    }

    std::uint32_t directions() noexcept {
        return core::mem_access::read<Base + offsets::output_status>();
    }

    void set_values(std::uint32_t mask, bool out_value) noexcept {
        if (out_value) {
            core::mem_access::write<Base + offsets::set_output_data>(mask);
        }
        else {
            core::mem_access::write<Base + offsets::clear_output_data>(mask);
        }
    }

    std::uint32_t values() noexcept {
        return core::mem_access::read<Base + offsets::pin_data_status>();
    }

    void enable_irqs(std::uint32_t mask) noexcept {
        core::mem_access::write<Base + offsets::irq_enable>(mask);
    }

    void disable_irqs(std::uint32_t mask) noexcept {
        core::mem_access::write<Base + offsets::irq_disable>(mask);
    }

    std::uint32_t irq_status() noexcept {
        return core::mem_access::read<Base + offsets::irq_status>();
    }

    irq_subscription make_subscription(std::uint32_t mask,
                                       eppostd::small_function<void(std::uint32_t)> fn) noexcept {
        return irq_server.make_subscription(mask, std::move(fn));
    }

    void subscribe_irq(irq_subscription &sub) noexcept {
        irq_server.subscribe(sub);
    }

    void irq_signal(std::uint32_t status) noexcept {
        if (status == 0) {
            return;
        }
        for (auto &s : irq_server.subs) {
            s.target_fn_(status);
        }
    }
};
} // namespace driver::gpio