#pragma once

#include <driver/irq/concept.hpp>
#include <driver/peripheral/concept.hpp>

namespace driver::gpio
{
namespace pin
{
enum class pull_config { none, up, down };

enum class direction { input, output };
} // namespace pin

template <class T>
concept bool GpioPeripheral = requires(T t) {
    requires(driver::peripheral::Peripheral<T>);
    {t.configure_direction(std::uint32_t{}, driver::gpio::pin::direction{})};
    {t.configure_pull(std::uint32_t{}, driver::gpio::pin::pull_config{})};
    { t.directions() }
    ->std::uint32_t;
    {t.set_values(std::uint32_t{}, bool{})};
    { t.values() }
    ->std::uint32_t;
};

template <class T>
concept bool GpioPin = requires(T t) {
    {t.configure_direction(pin::direction{})};
    {t.configure_pull(pin::pull_config{})};
    {t.set_value(bool{})};
    { t.value() }
    ->bool;
    {t.toggle()};
};
} // namespace driver::gpio