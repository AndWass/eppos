#pragma once

namespace driver::peripheral
{
template <class T>
concept bool Peripheral = requires(T t) {
    {t.enable()};
    {t.disable()};
};
} // namespace driver::peripheral