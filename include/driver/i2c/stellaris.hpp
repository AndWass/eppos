#pragma once

#include "i2c.hpp"
#include <core/peripheral_reg.hpp>

namespace driver::i2c::stellaris
{
template <std::size_t Base>
struct regs
{
    core::peripheral_reg<Base> slave_address;
    core::peripheral_reg<Base + 0x04, core::register_access::read_only> status;
    core::peripheral_reg<Base + 0x04, core::register_access::write_only> control;
    core::peripheral_reg<Base + 0x08> data;
    core::peripheral_reg<Base + 0x0C> timer_period;
    core::peripheral_reg<Base + 0x10> irq_mask;
    core::peripheral_reg<Base + 0x14, core::register_access::read_only> raw_interrupt_status;
    core::peripheral_reg<Base + 0x18, core::register_access::read_only> masked_interrupt_status;
    core::peripheral_reg<Base + 0x1C, core::register_access::write_only> irq_clear;
    core::peripheral_reg<Base + 0x20> configuration;
};

template <class T>
struct master : public driver::i2c::master
{
    T i2c_reg;
    void setup() {
        core::peripheral_reg<0x400FE000 + 0x104>{}.set_bits(12);
        core::peripheral_reg<0x400FE000 + 0x108>{}.set_mask(0x1f);
        core::peripheral_reg<0x40005000 + 0x420>{}.set_bits(0x04, 0x08);
        core::peripheral_reg<0x40005000 + 0x50C>{}.set_bits(0x04, 0x08);

        *i2c_reg.configuration = 0x10;
        *i2c_reg.timer_period = 9;
        *i2c_reg.irq_clear = 1;
    }

    virtual std::uint8_t slave_address() const override {
        return ((*i2c_reg.slave_address) >> 1) & 0x7f;
    }
    void set_slave_address(std::uint8_t slave, operation op) override {
        *i2c_reg.slave_address = slave << 1;
        if (op == operation::read) {
            *i2c_reg.slave_address |= 1;
        }
    }

    void reset() override {
        *i2c_reg.control = 0;
    }

    std::uint8_t read() const override {
        return *i2c_reg.data;
    }

    void write(std::uint8_t data) override {
        *i2c_reg.data = data;
    }

    void start() override {
        *i2c_reg.control = 0x03;
    }

    void ack_start() override {
        *i2c_reg.control = 0x0b;
    }

    void start_stop() override {
        *i2c_reg.control = 0x07;
    }

    void repeat_start() override {
        *i2c_reg.control = 0x01;
    }

    void ack_run() override {
        *i2c_reg.control = 0x09;
    }

    void stop() override {
        *i2c_reg.control = 0x04;
    }

    void stop_run() override {
        *i2c_reg.control = 0x05;
    }

    bool is_busy() const override {
        return *i2c_reg.status & 0x01;
    }

    bool is_error() const override {
        return *i2c_reg.status & 0x02;
    }

    bool is_arbitrition_lost() const override {
        return *i2c_reg.status & 0x10;
    }
};

} // namespace driver::i2c::stellaris