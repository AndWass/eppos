#pragma once

#include <cstdint>
#include <gsl/span>

namespace driver::i2c
{
struct master
{
    enum class operation : std::uint8_t { write, read };

    virtual void reset() = 0;
    virtual std::uint8_t slave_address() const = 0;
    virtual void set_slave_address(std::uint8_t slave, operation op) = 0;

    virtual std::uint8_t read() const = 0;
    virtual void write(std::uint8_t data);

    virtual void start() = 0;
    virtual void ack_start() = 0;
    virtual void start_stop() = 0;
    virtual void repeat_start() = 0;
    virtual void ack_run() = 0;
    virtual void stop() = 0;
    virtual void stop_run() = 0;

    virtual bool is_busy() const = 0;
    virtual bool is_error() const = 0;
    virtual bool is_arbitrition_lost() const = 0;

    virtual ~master() = default;

    virtual bool write(gsl::span<const std::uint8_t> data);
    virtual bool read(gsl::span<std::uint8_t> data);
    virtual bool write_read(gsl::span<const std::uint8_t> to_write,
                            gsl::span<std::uint8_t> read_buffer);

    struct chip
    {
        std::uint8_t slave_address;
        master *master_impl;
        chip(std::uint8_t slave_address, master *master_impl)
            : slave_address(slave_address), master_impl(master_impl) {
        }

        bool write(gsl::span<const std::uint8_t> data) {
            master_impl->set_slave_address(slave_address, master::operation::write);
            return master_impl->write(data);
        }

        bool read(gsl::span<std::uint8_t> buffer) {
            master_impl->set_slave_address(slave_address, master::operation::read);
            return master_impl->read(buffer);
        }

        bool write_read(gsl::span<const std::uint8_t> data, gsl::span<std::uint8_t> buffer) {
            master_impl->set_slave_address(slave_address, master::operation::write);
            return master_impl->write_read(data, buffer);
        }
    };
};
} // namespace driver::i2c