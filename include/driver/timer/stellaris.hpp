#pragma once

#include <core/peripheral_reg.hpp>

namespace driver::timer::stellaris
{
template <std::uint32_t Base>
struct regs
{
    core::peripheral_reg<Base> config;
    core::peripheral_reg<Base + 0x04> timerA_mode;
    core::peripheral_reg<Base + 0x08> timerB_mode;
    core::peripheral_reg<Base + 0x0C> control;
    core::peripheral_reg<Base + 0x18> irq_mask;
    core::peripheral_reg<Base + 0x1C, core::register_access::read_only> raw_irq_status;
    core::peripheral_reg<Base + 0x20, core::register_access::read_only> masked_irq_status;
    core::peripheral_reg<Base + 0x24, core::register_access::write_only> clear_irq;
    core::peripheral_reg<Base + 0x28> timerA_interval_load;
    core::peripheral_reg<Base + 0x2C> timerB_interval_load;
    core::peripheral_reg<Base + 0x48, core::register_access::read_only> timerA_value;
};

constexpr std::uint32_t timer0 = 0x40030000;
constexpr std::uint32_t timer1 = timer0 + 0x1000;
constexpr std::uint32_t timer2 = timer1 + 0x1000;

template <std::uint32_t timer>
struct timer32bit
{
    static regs<timer> &regs_() {
        static regs<timer> regs_impl;
        return regs_impl;
    }
    void setup() {
        core::peripheral_reg<0x400FE000 + 0x104> rcgc1;
        rcgc1.set_bits(16);
        auto &timer_regs = regs_();
        *timer_regs.config = 0;
        timer_regs.control.clear_bits(0);
    }
    void set_singleshot(bool single_shot) {
        auto &timer_regs = regs_();
        if (single_shot) {
            *timer_regs.timerA_mode = 1;
        }
        else {
            *timer_regs.timerA_mode = 2;
        }
    }

    bool timeout() const {
        return *regs_().raw_irq_status & 0x01;
    }

    void start(std::uint32_t timeout) {
        *regs_().timerA_interval_load = timeout;
        regs_().control.set_bits(0);
    }

    std::uint32_t current_value() {
        return *regs_().timerA_value;
    }
};
} // namespace driver::timer::stellaris