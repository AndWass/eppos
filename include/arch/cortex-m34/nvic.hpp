#include "board_config.hpp"
#include <core/mem_access.hpp>
namespace arch::cortexm34::nvic
{
namespace system_irqn
{
static constexpr int systick = -1;
}

static constexpr std::uint8_t lowest_priority = ((1 << BOARD_NVIC_PRIO_BITS) - 1) & 0x00FF;

static constexpr std::uint32_t nvic_iser_base = 0xe000e100;
static constexpr std::uint32_t nvic_icer_base = 0xe000e180;
static constexpr std::uint32_t nvic_ip_base = 0xe000e400;
static constexpr std::uint32_t scb_shp_base = 0xe000ed18;
inline void set_priority(int irq, std::uint8_t priority) {
    priority <<= 8 - BOARD_NVIC_PRIO_BITS;
    if (irq >= 0) {
        core::mem_access::write<std::uint8_t>(nvic_ip_base + irq, priority);
    }
    else {
        std::uint32_t offset = (irq & 0x0FUL) - 4;
        core::mem_access::write<std::uint8_t>(scb_shp_base + offset, priority);
    }
}

inline void enable_irq(int irq) {
    if (irq >= 0) {
        core::mem_access::write(nvic_iser_base + (irq >> 5), (1 << (irq & 0x1f)));
    }
}

inline void disable_irq(int irq) {
    if (irq >= 0) {
        core::mem_access::write(nvic_icer_base + (irq >> 5), (1 << (irq & 0x1f)));
    }
}
} // namespace arch::cortexm34::nvic