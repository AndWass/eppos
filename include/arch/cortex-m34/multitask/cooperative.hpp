#pragma once

#include "context.hpp"
#include <core/task_scheduler/task_control_block.hpp>
#include <cstdint>
#include <gsl/span>

namespace multitask
{
struct cooperative
{
    std::uint8_t yield_stack[128];

    void activate_tcb(core::task_scheduler::task_control_block *current,
                      core::task_scheduler::task_control_block &next)
        __attribute__((naked, noinline)) {

        arch::cortexm34::multitask::context::push();

        if (current) {
            asm volatile("mov %0, sp" : "=r"(current->stack_pointer));
        }

        // On first first stack we do some "magic" to ensure a task will end up in
        // end_task function if it ever returns from its main function.
        if (!next.is_started) {
            next.is_started = true;
            asm volatile("mov sp, %[next_stack]\r\n"
                         "mov r1, %[next_end]\r\n"
                         "pop {r4-r11, lr}\r\n"
                         "mov r0, lr\r\n"
                         "mov lr, r1\r\n"
                         "BX r0\r\n" ::[next_stack] "r"(next.stack_pointer),
                         [next_end] "r"(next.end));
        }
        asm volatile("mov sp, %[stack_ptr]\r\n"
                     "pop {r4-r11, pc}\r\n" ::[stack_ptr] "r"(next.stack_pointer));
    }
};
} // namespace multitask