#pragma once

#include <cstdint>

namespace arch::cortexm34::multitask::context
{
struct context
{
    std::uint32_t r4, r5, r6, r7, r8, r9, r10, r11, lr;
};
inline __attribute__((always_inline)) void push() {
    // Store the current context
    asm volatile("push {r4-r11, lr}\r\n");
}
} // namespace arch::cortexm34::multitask::context