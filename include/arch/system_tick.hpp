#include <chrono>

inline std::uint64_t operator""_kHz(unsigned long long hz) {
    return hz * 1000;
}

inline std::uint64_t operator""_MHz(unsigned long long hz) {
    return hz * 1000000ull;
}

namespace arch::system_tick
{
void start(std::uint64_t clk_frequency, std::chrono::microseconds timeout);
}