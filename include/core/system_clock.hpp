#include <chrono>
#include <eppostd/chrono/system_clock.hpp>

namespace core::system_clock
{
void tick(eppostd::chrono::system_clock::duration tick_interval);
}