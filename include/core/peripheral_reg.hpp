#pragma once

#include <cstdint>
#include <type_traits>

namespace core
{
namespace register_access
{
template <class... T>
constexpr bool only_integral_v = std::conjunction_v<std::is_integral<T>...>;

struct write_only
{
    using type = volatile uint32_t *const;

    static void set_mask(type ptr, std::uint32_t mask) {
        *ptr |= mask;
    }

    static void clear_mask(type ptr, std::uint32_t mask) {
        *ptr &= ~mask;
    }

    template <class... Bits>
    static void set_bits(type ptr, Bits... bits) {
        static_assert(only_integral_v<Bits...>, "Bits must only be integral values");
        using ui32 = std::uint32_t;
        std::uint32_t mask = (((static_cast<ui32>(bits) >= 0 && static_cast<ui32>(bits) <= 31)
                                   ? (1 << static_cast<ui32>(bits))
                                   : 0) |
                              ...);
        set_mask(ptr, mask);
    }

    template <class... Bits>
    static void clear_bits(type ptr, Bits... bits) {
        static_assert(only_integral_v<Bits...>, "Bits must only be integral values");
        using ui32 = std::uint32_t;
        std::uint32_t mask = (((static_cast<ui32>(bits) >= 0 && static_cast<ui32>(bits) <= 31)
                                   ? (1 << static_cast<ui32>(bits))
                                   : 0) |
                              ...);
        clear_mask(ptr, mask);
    }
};

struct read_only
{
    using type = volatile uint32_t const *const;
};

struct read_write : public write_only
{
    using type = volatile uint32_t *const;
};
} // namespace register_access

template <std::uintptr_t Addr, class Access = register_access::read_write>
class peripheral_reg
{
    using type = typename Access::type;
    typename Access::type ptr = reinterpret_cast<type>(Addr);

public:
    using value_type = decltype(*std::declval<type>());
    using reference = value_type &;
    using const_reference = const reference;

    static constexpr bool can_read = !std::is_same<Access, register_access::write_only>::value;
    static constexpr bool can_write = !std::is_same<Access, register_access::read_only>::value;

    reference operator*() {
        return *ptr;
    }

    value_type operator*() const {
        return *ptr;
    }

    void set_mask(std::uint32_t mask) {
        Access::set_mask(ptr, mask);
    }

    void clear_mask(std::uint32_t mask) {
        Access::clear_mask(ptr, mask);
    }

    template <class... Bits>
    void set_bits(Bits... bits) {
        Access::set_bits(ptr, bits...);
    }

    template <class... Bits>
    void clear_bits(Bits... bits) {
        Access::clear_bits(ptr, bits...);
    }
};
} // namespace core