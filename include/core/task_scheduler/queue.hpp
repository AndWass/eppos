#include <boost/intrusive/list.hpp>
namespace core::task_scheduler
{
namespace bint = boost::intrusive;
template <class MultitaskType>
struct queue
{
    struct scheduler_data
        : public bint::list_base_hook<bint::link_mode<bint::link_mode_type::normal_link>>
    {
        MultitaskType::multitasker_data multitask_data;
    };

    bint::list<task> task_list;
    typename bint::list<task>::iterator current_task;

    queue() : current_task(task_list.end()) {
    }
};
} // namespace core::task_scheduler