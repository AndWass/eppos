#pragma once

#include "task_control_block.hpp"
#include <eppostd/function.hpp>
#include <eppostd/intrusive_list.hpp>
#include <utility>

namespace core::task_scheduler
{
template <class MultitaskType>
struct round_robin
{
    using tcb_t = core::task_scheduler::task_control_block;
    struct task final : public eppostd::intrusive_list_base_hook
    {
        round_robin *task_owner = nullptr;
        tcb_t tcb;

        eppostd::function<bool()> is_blocked{[]() { return false; }};

        task() noexcept {};

        void join() {
            auto &current = task_owner->current();
            if (&current == this) {
                return;
            }
            auto join_block_check = [base = this]() { return base->task_owner->has_task(*base); };
            current.is_blocked = [base = this]() { return base->task_owner->has_task(*base); };
            task_owner->block_current();
            current.is_blocked = []() { return false; };
        }
    };

    MultitaskType multitask;

    eppostd::intrusive_list<task> task_list;
    eppostd::intrusive_list<task> blocked_tasks;

    typename eppostd::intrusive_list<task>::iterator current_task;

    round_robin() : current_task(task_list.end()) {
    }

    void add_task(task &tsk) {
        core::task_scheduler::reset(tsk.tcb);
        tsk.task_owner = this;
        task_list.push_back(tsk);
        if (current_task == task_list.end()) {
            current_task = task_list.begin();
        }
    }

    void remove_current() {
        auto to_remove = current_task;
        auto &next_task = next();
        task_list.erase(to_remove);
        for (auto iter = blocked_tasks.begin(); iter != blocked_tasks.end();) {
            if (!iter->is_blocked()) {
                auto &ref = *iter;
                iter = blocked_tasks.erase(iter);
                task_list.push_back(ref);
            }
            else {
                iter++;
            }
        }
        multitask.activate_tcb(nullptr, next_task.tcb);
    }

    void yield() {
        auto &from = current();
        auto &to = next();
        if (&from == &to) {
            // yielding to self so shortcircuit that one!
            return;
        }
        multitask.activate_tcb(&from.tcb, to.tcb);
    }

    void block_current() {
        auto &from_task = *current_task;
        current_task = task_list.erase(current_task);
        blocked_tasks.push_back(from_task);
        multitask.activate_tcb(&from_task.tcb, current_task->tcb);
    }

    void start() {
        auto &first = current();
        multitask.activate_tcb(nullptr, first.tcb);
    }

    task create_task(tcb_t::main_fn start_func, tcb_t::end_fn end_func,
                     std::uint8_t *stack_bottom) {
        task retval;
        retval.tcb.main = start_func;
        retval.tcb.end = end_func;
        retval.tcb.stack_bottom = stack_bottom;
        core::task_scheduler::reset(retval.tcb);
        return retval;
    }

    bool has_task(task &tsk) {
        for (auto &t : task_list) {
            if (&t == &tsk) {
                return true;
            }
        }
        for (auto &t : blocked_tasks) {
            if (&t == &tsk) {
                return true;
            }
        }

        return false;
    }

    task &next() {
        current_task++;
        if (current_task == task_list.end()) {
            current_task = task_list.begin();
        }
        return *current_task;
    }

    task &current() {
        return *current_task;
    }
};
} // namespace core::task_scheduler