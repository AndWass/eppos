#pragma once

#include <cstdint>

namespace core::task_scheduler
{
struct task_control_block
{
    typedef void (*main_fn)();
    typedef void (*end_fn)();

    main_fn main = nullptr;
    end_fn end = nullptr;

    std::uint8_t *stack_pointer = nullptr;
    std::uint8_t *stack_bottom = nullptr;

    bool is_started = false;
};
void reset(core::task_scheduler::task_control_block &tcb);
} // namespace core::task_scheduler