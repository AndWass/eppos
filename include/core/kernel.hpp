#pragma one

#include <arch/cortex-m34/multitask/cooperative.hpp>
#include <core/task_scheduler/round_robin.hpp>
#include <core/task_scheduler/task_control_block.hpp>
#include <core/user_task.hpp>

#define MEMBER_LAMBDA(name, val)                                                                   \
    decltype(val) name {                                                                           \
        val                                                                                        \
    }

namespace core::kernel
{
struct eppos
{
public:
    static eppos &instance() {
        static eppos inst;
        return inst;
    }
    using scheduler_t = task_scheduler::round_robin<multitask::cooperative>;

    scheduler_t scheduler;

    void yield() __attribute__((noinline)) {
        scheduler.yield();
    }

    void start() __attribute__((noinline)) {
        scheduler.add_task(kernel_task);
        scheduler.start();
    }

    void add_task(core::user_task<scheduler_t::task> &task) {
        scheduler.add_task(task.task_impl);
    }

    core::user_task<scheduler_t::task>
    create_task(core::task_scheduler::task_control_block::main_fn start_func,
                gsl::span<std::uint8_t> stack) {
        return {scheduler.create_task(start_func, on_end_task, stack.data() + stack.size())};
    }

    static void on_end_task() {
        // When a task is ending we want to remove
        // it from the scheduler
        auto &kernel = eppos::instance();
        kernel.scheduler.remove_current();
    }

private:
    // The kernel task main will never stop
    static void kernel_task_main() {
        auto &kernel = eppos::instance();
        while (true) {
            kernel.yield();
        }
    }

    scheduler_t::task kernel_task;
    std::array<std::uint8_t, 128> kernel_stack;
    eppos() {
        kernel_task = create_task(kernel_task_main, kernel_stack).task_impl;
    }
    eppos(const eppos &) = delete;
    eppos(eppos &&) = delete;
    eppos &operator=(const eppos &) = delete;
    eppos &operator=(eppos &&) = delete;
};
} // namespace core::kernel

namespace core::this_task
{
void yield() {
    ::core::kernel::eppos::instance().yield();
}
} // namespace core::this_task