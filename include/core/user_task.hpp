#pragma once

namespace core
{
template <class T>
struct user_task
{
    T task_impl;
    user_task() = default;
    user_task(T t) : task_impl(std::move(t)) {
    }
    void join() {
        task_impl.join();
    }
};
} // namespace core