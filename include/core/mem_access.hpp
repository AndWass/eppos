#pragma once

#include <cstdint>

namespace core::mem_access
{
template <std::uintptr_t Addr, class AccessType = std::uint32_t>
inline void write(AccessType value) noexcept {
    *reinterpret_cast<volatile AccessType *>(Addr) = value;
}

template <class AccessType = std::uint32_t>
inline void write(std::uintptr_t address, AccessType value) {
    *reinterpret_cast<volatile AccessType *>(address) = value;
}

template <std::uintptr_t Addr, class AccessType = std::uint32_t>
inline AccessType read() noexcept {
    return *reinterpret_cast<volatile AccessType const *>(Addr);
}

template <class AccessType = std::uint32_t>
inline AccessType read(std::uintptr_t address) {
    return *reinterpret_cast<volatile AccessType const *>(address);
}
} // namespace core::mem_access