#include "catch.hpp"
#include "eppostd/chrono/system_clock.hpp"

TEST_CASE("Epoch timepoint shall be 0", "[chrono]")
{
    auto now_ = eppostd::chrono::system_clock::now();
    REQUIRE(now_.time_since_epoch().count() == 0);
}


TEST_CASE("Adding ticks shall add to system clock", "[chrono]")
{
    auto now_ = eppostd::chrono::system_clock::now();
    REQUIRE(now_.time_since_epoch().count() == 0);
    core::chrono::system_clock_impl::add_ticks(std::chrono::milliseconds{10});
    REQUIRE(eppostd::chrono::system_clock::now().time_since_epoch().count() == 10000);
}