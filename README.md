# eppOS

Embedded C++ OS. This is not an OS (yet), but hopefully one day.
It is my little project and general sandbox to try out stuff
regarding C++ in an embedded setting. See [my blog](https://andwass.gitlab.io/blog/)
for more on the same subject.