#include <cstdint>

void i2c_init();
void ssd0303_init();
void ssd0303_update();
void ssd0303_set_column(unsigned int x, unsigned int row, std::uint8_t col);

[[noreturn]] int main() {
    i2c_init();
    ssd0303_init();

    for(int i=0; i<132; i++) {
        ssd0303_set_column(i, 0, 0x11);
        ssd0303_set_column(i, 1, 0x11);
    }
    ssd0303_update();

    while(true);
}

extern "C" void SystemInit() {}