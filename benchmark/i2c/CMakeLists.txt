add_executable(benchmark_i2c_minimal minimal.cpp ${BOARD_SRC})
add_executable(benchmark_i2c_no_lto no_lto.cpp ${BOARD_SRC} i2c.cpp
    ssd0303.cpp)