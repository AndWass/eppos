#include <cstdint>

bool i2c_write(const std::uint8_t*, std::size_t);

void ssd0303_init() {
    std::uint8_t p[2] = {0x80, 0xAF};
    i2c_write(p, 2);
    p[1] = 0xA4;
    i2c_write(p, 2);
    p[1] = 0xA6;
    i2c_write(p, 2);
}

static std::uint8_t pixels[2][133];

void ssd0303_set_column(unsigned int x, unsigned int row, std::uint8_t col) {
    pixels[row % 2][1 + x % 132] = col;
}

void ssd0303_update() {
    pixels[0][0] = 0x40;
    pixels[1][0] = 0x40;

    std::uint8_t temp[2] = {0x80, 0x00};
    i2c_write(pixels[0], 133);
    i2c_write(temp, 2);
    temp[1] = 0x10;
    i2c_write(temp, 2);
    temp[1] = 0xb1;
    i2c_write(temp, 2);
    i2c_write(pixels[1], 133);
}