#include <cstdint>

#define REG(name, addr) volatile std::uint32_t* name = (std::uint32_t*)(addr)
#define REG_WRITE(addr, value) *((volatile std::uint32_t*)(addr)) = value

static constexpr std::uint32_t i2c_base = 0x40020000;

REG(slave_address, i2c_base);
REG(status_control, i2c_base + 0x04);
REG(data, i2c_base + 0x08);
REG(timer_period, i2c_base + 0x0C);
REG(config, i2c_base + 0x20);

void init_i2c() {
    REG_WRITE(0x400FE000+0x104, 12);
    REG_WRITE(0x400FE000+0x108, 0x1f);
    REG_WRITE(0x40005000+0x420, 12);
    REG_WRITE(0x40005000+0x50C, 12);

    *config = 0x10;
    *timer_period = 9;
}

bool write_data(const std::uint8_t *p, std::size_t length) {
    *slave_address = 0x7a;
    if(length == 0) {
      return true;
  }
  if(length == 1) {
      *data = p[0];
      *status_control = 0x07;
      while(*status_control & 0x01);
      return !(*status_control & 0x02);
  }

  *data = p[0];
  *status_control = 0x03;

  for (std::size_t i = 1; i < length; i++) {
    while(*status_control & 0x01);
    if (*status_control & 0x02) {
      *status_control = 0x04;
      return false;
    }

    if (i + 1 == length) {
      *data = p[i];
      *status_control = 0x05;
    } else {
      *data = p[i];
      *status_control = 0x01;
    }
  }
  while(*status_control & 0x01);
  *status_control = 0x04;

  return !(*status_control & 0x02);
}

void init_ssd0303() {
    std::uint8_t p[2] = {0x80, 0xAF};
    write_data(p, 2);
    p[1] = 0xA4;
    write_data(p, 2);
    p[1] = 0xA6;
    write_data(p, 2);
}

static std::uint8_t pixels[2][133];

[[noreturn]] int main()
{
    init_i2c();
    init_ssd0303();

    pixels[0][0] = 0x40;
    pixels[1][0] = 0x40;

    std::uint8_t temp[2] = {0x80, 0x00};

    for(int y=0; y<2; y++) {
        for(int x=0; x<96; x++) {
            pixels[y][1+x] = 0x11;
        }
    }

    write_data(pixels[0], 133);
    write_data(temp, 2);
    temp[1] = 0x10;
    write_data(temp, 2);
    temp[1] = 0xb1;
    write_data(temp, 2);
    write_data(pixels[1], 133);

    while(true);
}

extern "C" void SystemInit() {}