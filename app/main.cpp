#include "arch/system_tick.hpp"
#include "core/kernel.hpp"

#include "driver/gpio/atsam4s_pio.hpp"
#include <driver/display/ssd0303.hpp>
#include <driver/gpio/pin.hpp>
#include <driver/spi/sw_spi.hpp>

#include "eppostd/chrono/chrono.hpp"

#include <arch/cortex-m34/nvic.hpp>

using at_pioc = driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_c>;

#include "ATSAM4SD32C.h"

#define printf(...)
//#if 0
static core::kernel::eppos &kernel = core::kernel::eppos::instance();

static std::uint8_t main_stack[512];
static std::uint8_t second_stack[512];
static std::uint8_t third_stack[512];

void task_main();
void second_main();
void third_main();

using task_t = decltype(kernel.create_task(nullptr, main_stack));
task_t main_task;
task_t second_task;
task_t third_task;
void task_main() {
    auto &pioc = at_pioc::instance();
    auto pc20 = driver::gpio::pin::make_pin(pioc, 20);
    int test = 5;
    for (;;) {
        auto now = eppostd::chrono::system_clock::now();
        auto stop_time = now + 1s;
        while (eppostd::chrono::system_clock::now() < stop_time) {
            core::this_task::yield();
        }
        pc20.toggle();
    }
}

void second_main() {
    auto &pioa = driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_a>::instance();
    auto pa16 = driver::gpio::pin::make_pin(pioa, 16);
    pioa.enable();
    pa16.configure_direction(driver::gpio::pin::direction::output);
    for (;;) {
        auto now = eppostd::chrono::system_clock::now();
        auto stop_time = now + 1333ms;
        while (eppostd::chrono::system_clock::now() < stop_time) {
            core::this_task::yield();
        }
        pa16.toggle();
    }
}

void third_main() {
    auto &pioc = driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_c>::instance();
    auto pc22 = driver::gpio::pin::make_pin(pioc, 22);
    int test = 8546;
    printf("Third main %d\n", test++);
    for (;;) {
        auto now = eppostd::chrono::system_clock::now();
        auto stop_time = now + 781ms;
        while (eppostd::chrono::system_clock::now() < stop_time) {
            core::this_task::yield();
        }
        pc22.toggle();
    }
    printf("After third main yield %d\n", test);
}

#include <atomic>
#include <mutex>

namespace kstd
{
class spinlock_mutex
{
    std::atomic_flag lock_ = ATOMIC_FLAG_INIT;

public:
    constexpr spinlock_mutex() noexcept = default;
    spinlock_mutex(const spinlock_mutex &) = delete;

    void lock() noexcept {
        while (lock_.test_and_set(std::memory_order_acquire)) // acquire lock
            ;                                                 // spin
    }
    bool try_lock() noexcept {
        if (lock_.test_and_set(std::memory_order_acquire)) {
            return false;
        }
        return true;
    }

    void unlock() noexcept {
        lock_.clear(std::memory_order_release);
    }
};
} // namespace kstd

[[noreturn]] int main() {
    WDT->WDT_MR = (1 << 15);
    /*CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;*/
    if (!(CMCC->CMCC_SR & 0x01)) {
        CMCC->CMCC_CTRL = 0x01;
    }

    EFC0->EFC0_FMR |= (0xf00);

    PMC->CKGR_MOR |= (0x00370000 | (2 << 4));
    while (!(PMC->PMC_SR & (1 << 16)))
        ;
    PMC->CKGR_PLLAR = (1 << 29) | (7 << 16) | (0x3f << 8) | 8;
    while (!(PMC->PMC_SR & 0x02))
        ;
    PMC->PMC_MCKR = 2;
    while (!(PMC->PMC_SR & 0x08))
        ;
    auto &pioc = driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_c>::instance();
    auto &pioa = driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_a>::instance();
    ;
    pioa.enable();
    pioc.enable();

    auto pc22 = driver::gpio::pin::make_pin(pioc, 22);
    auto pc20 = driver::gpio::pin::make_pin(pioc, 20);
    pc22.configure_direction(driver::gpio::pin::direction::output);
    pc20.configure_direction(driver::gpio::pin::direction::output);
    auto pc29 = driver::gpio::pin::make_pin(pioc, 29);
    pc29.enable_irq();
    auto irq_sub = pioc.make_subscription(1 << 29, [&pc20, &pc22, &pc29](auto) {
        // pc20.toggle();
        // pc22.toggle();
        pc20.set_value(pc29.value());
        pc22.set_value(pc29.value());
    });
    pioc.subscribe_irq(irq_sub);

    arch::cortexm34::nvic::enable_irq(PIOC_IRQn);

    auto disp_reset = driver::gpio::pin::make_pin(pioc, 31);
    disp_reset.configure_direction(driver::gpio::pin::direction::output);
    disp_reset.set_value(true);

    auto mosi = driver::gpio::pin::make_pin(pioa, 13);
    mosi.configure_direction(driver::gpio::pin::direction::output);

    auto clk = driver::gpio::pin::make_pin(pioa, 14);
    clk.configure_direction(driver::gpio::pin::direction::output);
    clk.set_value(false);

    auto cs = driver::gpio::pin::make_pin(pioa, 10);
    cs.configure_direction(driver::gpio::pin::direction::output);
    cs.set_value(true);

    auto dc = driver::gpio::pin::make_pin(pioc, 21);
    dc.configure_direction(driver::gpio::pin::direction::output);
    dc.set_value(true);
    disp_reset.set_value(true);

    auto spi = driver::spi::make_sw_spi(clk, mosi, mosi);
    auto disp = driver::display::ssd1306::make_ssd1306(spi, cs, dc);
    disp.setup();
    /*for(int i=0; i<10; i++) {
        pc22.toggle();
        for(int i=0; i<100000; i++) {
            (void)pc22.value();// ^= (1 << 22);
        }
    }*/
    main_task = kernel.create_task(task_main, main_stack);
    second_task = kernel.create_task(second_main, second_stack);
    third_task = kernel.create_task(third_main, third_stack);

    kernel.add_task(main_task);
    kernel.add_task(second_task);
    kernel.add_task(third_task);

    arch::system_tick::start(12_MHz / 8, 100us);

    printf("Starting kernel...\n");
    kernel.start();
    for (;;)
        ;
}
//#endif

#if 0
[[noreturn]] int main() {
    WDT->WDT_MR = (1 << 15);
    PMC->CKGR_PLLAR = (1 << 29) | (5 << 16) | (0x3f << 8) | 1;
    while(!(PMC->PMC_SR & 0x02));
    PMC->PMC_MCKR = 2;
    while(!(PMC->PMC_SR & 0x08));
    auto& pioc = driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_c>::instance();
    pioc.enable();
    auto pc20 = driver::gpio::pin::make_pin(pioc, 20);
    pc20.configure_direction(driver::gpio::pin::direction::output);
    auto pc22 = driver::gpio::pin::make_pin(pioc, 22);
    auto pc29 = driver::gpio::pin::make_pin(pioc, 29);
    pc29.configure_pull(driver::gpio::pin::pull_config::up);
    pc22.configure_direction(driver::gpio::pin::direction::output);
    pc20.configure_direction(driver::gpio::pin::direction::output);
    pc20.set_value(false);
    pc22.set_value(false);
    pc29.enable_irq();
    auto sub = pioc.make_subscription(1 << 29, [&pioc, &pc22, &pc29](std::uint32_t status) {
        pc22.set_value(pc29.value());
    });
    pioc.subscribe(sub);
    NVIC_EnableIRQ(PIOC_IRQn);
    for(;;) {
        //pc22.toggle();
    }
}
#endif