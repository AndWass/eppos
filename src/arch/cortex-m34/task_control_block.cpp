#include <core/task_scheduler/task_control_block.hpp>
#include <cstdint>

static std::uint8_t *push8(std::uint8_t *stack_ptr, std::uint8_t value) {
    stack_ptr--;
    *stack_ptr = value;
    return stack_ptr;
}

static std::uint8_t *push16(std::uint8_t *stack_ptr, std::uint16_t value) {
    stack_ptr = push8(stack_ptr, value >> 8);
    return push8(stack_ptr, value & 0x00FF);
}

static std::uint8_t *push32(std::uint8_t *stack_ptr, std::uint32_t value) {
    stack_ptr = push16(stack_ptr, value >> 16);
    return push16(stack_ptr, value & 0x00FFFF);
}

namespace core::task_scheduler
{
void reset(core::task_scheduler::task_control_block &tcb) {
    auto lr = reinterpret_cast<std::uint32_t>(tcb.main);
    tcb.stack_pointer = push32(tcb.stack_bottom, lr | 0x01);
    tcb.stack_pointer -= 4 * 8;
}
} // namespace core::task_scheduler