#include "arch/cortex-m34/nvic.hpp"
#include <core/mem_access.hpp>
#include <core/system_clock.hpp>
#include <cstdint>
#include <eppostd/chrono/chrono.hpp>

static constexpr std::uint32_t systick_control_status = 0xe000e010;
static constexpr std::uint32_t systick_reload_value = systick_control_status + 0x04;
static constexpr std::uint32_t systick_current_value = systick_reload_value + 0x04;

static std::chrono::microseconds tick_interval_us{0};

namespace arch::system_tick
{
void start(std::uint64_t clk_frequency, std::chrono::microseconds timeout) {
    tick_interval_us = timeout;

    auto tick_interval = eppostd::chrono::duration_to_ticks(clk_frequency, timeout);
    namespace nvic = arch::cortexm34::nvic;
    core::mem_access::write<systick_reload_value>(tick_interval - 1);
    nvic::set_priority(nvic::system_irqn::systick, nvic::lowest_priority);
    core::mem_access::write<systick_current_value>(0);
    core::mem_access::write<systick_control_status>(3); // Enable SysTick and systick exceptions
}
} // namespace arch::system_tick

extern "C" void SysTick_Handler() {
    core::system_clock::tick(tick_interval_us);
}