#include <driver/i2c/i2c.hpp>

static void wait_while_busy(driver::i2c::master *i2c) {
    while (i2c->is_busy())
        ;
}

static bool write_no_stop(driver::i2c::master *i2c, gsl::span<const std::uint8_t> data) {
    if (data.size() == 0) {
        return true;
    }
    if (data.size() == 1) {
        i2c->write(data[0]);
        i2c->start();
        wait_while_busy(i2c);
        return !i2c->is_error();
    }

    i2c->write(data[0]);
    i2c->start();
    for (std::size_t i = 1; i < data.size(); i++) {
        wait_while_busy(i2c);
        if (i2c->is_error()) {
            if (i2c->is_arbitrition_lost()) {
                i2c->reset();
            }
            else {
            }
            return false;
        }
        i2c->write(data[0]);
        i2c->repeat_start();
    }
    wait_while_busy(i2c);

    return !i2c->is_error();
}

namespace driver::i2c
{
bool master::write(gsl::span<const std::uint8_t> data) {
    if (data.size() == 0) {
        return true;
    }
    if (data.size() == 1) {
        write(data[0]);
        start_stop();
        wait_while_busy(this);
        return !is_error();
    }

    write(data[0]);
    start();
    for (std::size_t i = 1; i < data.size(); i++) {
        wait_while_busy(this);
        if (is_error()) {
            if (is_arbitrition_lost()) {
                reset();
            }
            else {
                stop();
            }
            return false;
        }

        if (i + 1 == data.size()) {
            write(data[i]);
            stop_run();
        }
        else {
            write(data[i]);
            repeat_start();
        }
    }
    wait_while_busy(this);
    stop();

    return !is_error();
}

bool i2c::master::read(gsl::span<std::uint8_t> data) {
    if (data.size() == 0) {
        return true;
    }
    if (data.size() == 1) {
        start_stop();
        wait_while_busy(this);
        data[0] = read();
        return !is_error();
    }

    ack_start();
    std::size_t i = 0;
    for (i = 0; i < data.size() - 1; i++) {
        wait_while_busy(this);
        if (is_error()) {
            if (is_arbitrition_lost()) {
                reset();
            }
            else {
                stop();
            }
            return false;
        }

        data[i] = read();

        if (i == data.size() - 2) {
            stop_run();
        }
        else {
            ack_run();
        }
    }
    wait_while_busy(this);
    data[i] = read();

    return !is_error();
}

bool master::write_read(gsl::span<const std::uint8_t> to_write,
                        gsl::span<std::uint8_t> read_buffer) {
    auto retval = write(to_write);
    set_slave_address(slave_address(), operation::read);
    retval = retval && read(read_buffer);
    if (!retval) {
        reset();
    }
    return retval;
}
} // namespace driver::i2c
