#include <driver/display/ssd0303.hpp>

namespace driver::display
{
ssd0303::ssd0303(std::uint8_t i2c_address, driver::i2c::master *i2c_bus)
    : chip(i2c_address, i2c_bus) {
    pixel_command_buffer[0].fill(0);
    pixel_command_buffer[1].fill(0);
    // This is the write pixel command, and will never be changed
    pixel_command_buffer[0][0] = 0x40;
    pixel_command_buffer[1][0] = 0x40;
}

void ssd0303::setup() {
    send_command(0xAF);
    send_command(0xA4);
    send_command(0xA6);
}

void ssd0303::send_command(std::uint8_t command) {
    std::uint8_t data[2] = {0x80, command};
    chip.write(data);
}

void ssd0303::set_pixel(unsigned int x, unsigned int y, bool on) {
    if (x >= 132 || y >= 16) {
        return;
    }
    auto byte_row = y / 8;
    auto bit_row = y % 8;
    if (on) {
        pixel_command_buffer[byte_row][1 + x] |= (1 << bit_row);
    }
    else {
        pixel_command_buffer[byte_row][1 + x] &= ~(1 << bit_row);
    }
}
void ssd0303::update() {
    chip.write(pixel_command_buffer[0]);
    send_command(0x00);
    send_command(0x10);
    send_command(0xb1);
    chip.write(pixel_command_buffer[1]);
}
} // namespace driver::display