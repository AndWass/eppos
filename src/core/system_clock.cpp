#include <core/system_clock.hpp>

namespace core::system_clock
{
void tick(eppostd::chrono::system_clock::duration tick_interval) {
    eppostd::chrono::detail::system_clock_impl::value() += tick_interval;
}
} // namespace core::system_clock