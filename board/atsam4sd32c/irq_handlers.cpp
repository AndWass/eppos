/// Use this file for board-specific IRQ handlers.
/// Architecture-specific handlers, such as SysTick_Handler
/// etc. must not go in this file! They are defined in 
/// src/arch/cortex-m34.

#include "driver/gpio/atsam4s_pio.hpp"

extern "C" void PIOA_Handler() {
    static auto &pioa =
        driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_a>::instance();
    pioa.irq_signal(pioa.irq_status());
}

extern "C" void PIOB_Handler() {
    static auto &piob =
        driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_b>::instance();
    piob.irq_signal(piob.irq_status());
}

extern "C" void PIOC_Handler() {
    static auto &pioc =
        driver::gpio::atsam4s_pio<driver::gpio::atsam4s_pio_base::pio_c>::instance();
    pioc.irq_signal(pioc.irq_status());
}